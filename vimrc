" Disable arrow keys in escape mode
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" Disable arrow keys in insert mode
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>

" Enable line numbers
set number

" Change tab size to 2
 set tabstop=2
 set shiftwidth=2
 set expandtab

" Disable colorscheme 
:syntax off

" Change color of line numbers
highlight LineNr ctermfg=grey
:highlight LineNr guifg=#050505

" Forbidden +80 characters
let &colorcolumn=join(range(80,81),",")

" Disable color of brackets
hi MatchParen cterm=none ctermbg=black ctermfg=white